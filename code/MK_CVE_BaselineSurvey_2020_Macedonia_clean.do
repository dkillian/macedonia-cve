**********************************************************************************************************		
	* 	Gustavo Castillo
	*	This do file: 
		*Labels and organizes variables
		*Performs logic checks
	*	Dec 2020									    		
	*	Uses: .dta
	* 	Saves: MK_CVE_BaselineSurvey_2020_raw_Macedonia_clean.dta
**************************************************************************************************************

*set working DIRECTORIES 	
global dirfolder "C:\Users\gustavo.castillo\Desktop\CVE Midline Survey Questionnaire for Bosnia and Herzegovina" 

*IMPORT data
clear
use "$dirfolder\Data\Midline\raw data\MK_CVE_BaselineSurvey_2020_raw.dta", clear

*CHECK import of all necessary variables
ssc install unique
unique SbjNum // 3390 unique codes

*CONVERT strings to numeric 
gen EnumGender1=.
replace EnumGender1=1 if Gender=="Male"
replace EnumGender1=2 if Gender=="Female"

label def gend 1 "Male" 2 "Female"
label val EnumGender1 gend

replace Ethniity="1" if Ethniity=="Albanian"
replace Ethniity="2" if Ethniity=="Macedonian"
destring Ethniity, replace

label def ethnicityy 1 "Albanian" 2 "Macedonian"
label val Ethniity ethnicityy

gen SupervGender1=.
replace SupGender="1" if SupGender=="Male"
replace SupGender="2" if SupGender=="Female"
destring SupGender, replace
label val SupGender gend

replace SupEth="1" if SupEth=="Albanian"
replace SupEth="2" if SupEth=="Macedonian"
destring SupEth, replace
label val SupEth ethnicityy

*Checking for conditional - skip patterns and logic checks
tab Q1,m
tab Q1_opn if Q1==8 

tab Q2,m
tab Q2a if Q2==1

tab Q12,m
tab Q12_opn if Q12==10

tab Q12,m
tab Q12_opn if Q12==8

tab Q19,m
tab Q19a if Q19==1 

tab Q19a,m
tab Q19a_opn if Q19a==9

tab Q22,m
tab Q22a1_1 if Q22==2 | Q22==3 | Q22==4

tab Q22a1_1,m
tab Q22a1_2,m
tab Q22a1_3,m
tab Q22a1_opn if Q22a1_1==5 | Q22a1_2==5 | Q22a1_3==5

tab Q23,m
tab Q23a1_1 if Q23==2 | Q23==3 | Q23==4

tab Q23a1_1,m
tab Q23a1_2,m
tab Q23a1_3,m
tab Q23a1_opn if Q23a1_1==5 | Q23a1_2==5 | Q23a1_3==5 // The number of obs in Q23a1_opn does not equal the total number of obs in "Other" for Q23a1_1, Q23a1_2, Q_23a3 - it is a much higher number. Several observations for other options for Q23a1_1 (for example, friends) have the Q23a1_opn option filled out.

tab Q31,m
tab Q31a if Q31==1

tab Q31,m
tab Q31b if Q31==1 

tab Q32,m
tab Q32a if Q32==1

tab Q33,m
tab Q33a_1 if Q33==2 | Q33==3 | Q33==4 | Q33==5 | Q33==6

tab Q33a_1,m
tab Q33b_O1 if Q33a_1==1

tab Q33b_O1,m
tab Q33b1_opn if Q33b_O1==1

tab Q33b_O1,m
tab Q33b_1_cod if Q33b_O1==1

tab Q33,m
tab Q33a_2 if Q33==2 | Q33==3 | Q33==4 | Q33==5 | Q33==6

tab Q33a_2,m
tab Q33b_O2 if Q33a_2==1

tab Q33b_O2,m
tab Q33b2_opn if Q33b_O2==1

tab Q33b_O2,m
tab Q33b_2_cod if Q33b_O2==1

tab Q33,m
tab Q33a_3 if Q33==2 | Q33==3 | Q33==4 | Q33==5 | Q33==6

tab Q33a_3,m
tab Q33b_O3 if Q33a_3==1

tab Q33b_O3,m
tab Q33b3_opn if Q33b_O3==1

tab Q33b_O3,m
tab Q33b_3_cod if Q33b_O3==1

tab Q33,m
tab Q33a_4 if Q33==2 | Q33==3 | Q33==4 | Q33==5 | Q33==6

tab Q33a_4,m
tab Q33b_O4 if Q33a_4==1

tab Q33b_O4,m
tab Q33b4_opn if Q33b_O4==1

tab Q33b_O4,m
tab Q33b_4_cod if Q33b_O4==1

tab Q33,m
tab Q33a_5 if Q33==2 | Q33==3 | Q33==4 | Q33==5 | Q33==6

tab Q33a_5,m
tab T_51_5_S if Q33a_5==1

tab Q33b_O5,m
tab Q33b5_opn if Q33b_O5==1

tab Q33b_O5,m
tab Q33b_5_cod if Q33b_O5==1

tab Q36,m
tab Q36_opn if Q36==6

tab Q38,m
tab Q38_opn if Q38==7

tab Q38,m
tab Q41 if Q38!=5 | Q38!=6

tab Q38,m
tab Q42 if Q38!=5 | Q38!=6

tab Q38,m
tab Q43_1 if Q38!=5 | Q38!=6

tab Q38,m
tab Q43_2 if Q38!=5 | Q38!=6

tab Q38,m
tab Q44 if Q38!=5 | Q38!=6

tab Q38,m
tab Q45 if Q38!=5 | Q38!=6

tab Q38,m
tab Q46 if Q38==1

tab Q54,m
tab Q54x if Q54==1

save"$dirfolder\Data\Midline\01_preprocessing\MK_CVE_BaselineSurvey_2020_raw_Macedonia_clean", replace
