**********************************************************************************************************		
	* 	Gustavo Castillo
	*	This do file: 
		*Performs exploratory analysis
	*	Jan 2021									    		
	*	Uses: .dta
	* 	Saves: MK_CVE_BaselineSurvey_2020_Macedonia_analysis.dta
**************************************************************************************************************

*set working DIRECTORIES 	
global dirfolder "C:\Users\gustavo.castillo\Desktop\CVE Midline Survey Questionnaire for Bosnia and Herzegovina" 

*CREATE log file

log using "VE Model Macedonia.txt", text append

*IMPORT data
clear
use "$dirfolder\Data\Midline\01_preprocessing\MK_CVE_BaselineSurvey_2020_Macedonia_prep.dta", clear

sum Q30_1 Q30_2	Q30_3 Q30_4	Q30_5 Q31a_positive	Q31b_positive Q32a_positive	Q46_bin_jihad Q22 Q23 Q13 Q16_1_optimistic Q17_1_satisfied	Q17_2_satisfied	Q17_3_satisfied	Q17_4_satisfied	Q17_5_satisfied	Q19a Q19a_opn Q44_bin Q37_bin Q45_bin Q14_bin Q15_bin Q25_bin Q8_1_trust Q8_2_trust	Q8_3_trust Q8_4_trust Q8_5_trust Q28 Q29 Q22 Q22a1_1 Q23 Q23a1_1 Q33_bin Q33a_1 Q33a_2 Q33a_3	Q33a_4	Q33a_5	Q33b_1_cod	Q33b_2_cod	Q33b_3_cod	Q33b_4_cod	Q33b_5_cod	Q4_1_trust	Q4_2_trust Q4_3_trust Q4_4_trust Q7_1_agree	Q2 Q2a_grp	Q40_bin	Q1_religion	Q41_bin	Q42_bin	Q10_1_agree	Q10_2_agree	Q10_3_agree	Q11_1_agree	Q11_2_agree	Q43_1_agree	Q43_2_agree	age	female	Q56	ethnicity2	Q55_bin	Q54 Q39_1 Q39_2 Q39_3 Q39_4	Q39_5 Q39_6	Q39_7 Q39_8	Q39_9 Q39_10 Q12_bin_uni_student Q57_bin_daily Q12_bin_unemployed Q58_1 Q58_2 Q58_3

log close


*Bivariate Regression - Linear Probability Model
reg Q46_bin_jihad age
reg Q46_bin_jihad female
reg Q46_bin_jihad Q56
reg Q46_bin_jihad i.ethnicity // (?) only shows Albanian
reg Q46_bin_jihad Q55_bin
reg Q46_bin_jihad Q54 // s - did you attend rel. shcool
reg Q46_bin_jihad Q39_1
reg Q46_bin_jihad Q39_2
reg Q46_bin_jihad Q39_3
reg Q46_bin_jihad Q39_4
reg Q46_bin_jihad Q39_5 // where did you acq knowl. of rel. public sch teach
reg Q46_bin_jihad Q39_6
reg Q46_bin_jihad Q39_7
reg Q46_bin_jihad Q39_8 // Relig study circles
reg Q46_bin_jihad Q39_9 // NGO
reg Q46_bin_jihad Q39_10
reg Q46_bin_jihad Q12_bin_uni_student
reg Q46_bin_jihad Q57_bin_daily // surf int daily
reg Q46_bin_jihad Q12_bin_unemployed
reg Q46_bin_jihad Q58_1
reg Q46_bin_jihad Q58_2
reg Q46_bin_jihad Q58_3


*Multiple Linear Regressions

reg Q46_bin_jihad age female Q56 Q55_bin Q39_1 Q39_2 Q39_3 // Q39_1 = 11
reg Q46_bin_jihad age female Q56 Q55_bin Q54 Q39_1 Q39_2 Q39_3 Q39_4  // Q54 = 19 
reg Q46_bin_jihad age female Q56 Q55_bin Q54 Q39_1 Q39_2 Q39_3 Q39_4 
reg Q46_bin_jihad Q39_5 Q39_6 Q39_7 Q39_8 Q39_9 Q39_10 
reg Q46_bin_jihad Q12_bin_uni_student Q57_bin_daily Q12_bin_unemployed Q58_1 Q58_2 Q58_3 // Q39_5 = 19 and Q39_7
reg Q46_bin_jihad age female Q56 Q12_bin_uni_student Q57_bin_daily Q12_bin_unemployed Q39_1 Q39_2 Q39_3 Q58_1 Q58_2 Q58_3 // female =36% , unemployed = 81%, 39_2, 
reg Q46_bin_jihad age female Q56 Q54 Q39_4 Q39_5 Q39_6 Q39_7 Q12_bin_uni_student Q57_bin_daily Q12_bin_unemployed

reg Q46_bin_jihad age female Q56 Q39_1 Q39_2 Q39_3 Q39_4 Q39_5 Q39_6 Q39_7 Q39_8 Q39_9 Q39_10 //female becomes not stat sig, 39_8 39_2
reg Q46_bin_jihad age female Q56 Q58_1 Q58_2 Q58_3
reg Q46_bin_jihad age female Q56 Q12_bin_uni_student Q57_bin_daily Q12_bin_unemployed

*Multiple Linear Regressions (adding factors)
reg Q46_bin_jihad age female Q13 Q16_1_optimistic Q19a Q44_bin Q8_1_trust
reg Q46_bin_jihad age female Q17_1_satisfied  // Q17_1 = 6.8
reg Q46_bin_jihad age female Q17_2_satisfied Q8_3_trust
reg Q46_bin_jihad age female Q17_3_satisfied Q8_4_trust // Q8_4_trust 7%
reg Q46_bin_jihad age female Q17_4_satisfied Q8_5_trust
reg Q46_bin_jihad age female Q17_5_satisfied Q28 Q29 Q22 Q22a1_1
reg Q46_bin_jihad age female Q45_bin Q23 Q23a1_1 Q33_bin Q33a_1
reg Q46_bin_jihad age female Q14_bin Q33a_2 Q2a_grp
reg Q46_bin_jihad age female Q15_bin Q33a_3 Q40_bin
reg Q46_bin_jihad age female macedonian_bin Q33_bin Q25_bin Q1_religion // Q15_bin = 18%
coefplot, drop(_cons) xline(0)

reg Q46_bin_jihad age female Q33b_1_cod Q41_bin // Q41_bin = 25
reg Q46_bin_jihad age female Q33b_2_cod Q4_2_trust Q42_bin 
reg Q46_bin_jihad age female Q33b_3_cod Q4_3_trust Q10_1_agree Q11_1_agree // Q10_1_ gree = 93%
reg Q46_bin_jihad age female Q4_4_trust Q10_2_agree Q11_2_agree
reg Q46_bin_jihad age female Q7_1_agree Q10_3_agree Q43_1_agree
reg Q46_bin_jihad age female Q4_1_trust Q2 Q43_2_agree // Q4_1_trust = 11%

*Save
save "$dirfolder\Data\Midline\02_analysis\MK_CVE_BaselineSurvey_2020_Macedonia_analysis.dta", replace
