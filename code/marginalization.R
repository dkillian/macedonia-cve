
names(dat)

frq(dat$ethnicity)
frq(dat$ethnicity2)
frq(dat$religion2)
frq(dat$Q37_often)
frq(dat$maj_rel)

tab_xtab(dat$religion2, dat$ethnicity, show.col.prc = T)

q37_eth <- svyrdat %>%
  group_by(ethnicity2) %>%
  summarise(percent=survey_mean(Q37_often, na.rm=T)) %>%
  na.omit() %>%
  mutate(Type="Ethnic marginalization",
         Ethnicity = get_labels(dat$ethnicity2),
         Margin = 1.96* percent_se) %>%
  select(Type, Ethnicity, Percent=percent, Margin)

q37_eth

frq(dat$Q44_often)

q44_eth <- svyrdat %>%
  group_by(ethnicity2) %>%
  summarise(percent=survey_mean(Q44_often, na.rm=T)) %>%
  na.omit() %>%
  mutate(Type="Religious marginalization",
         Ethnicity = get_labels(dat$ethnicity2),
         Margin = 1.96* percent_se) %>%
  select(Type, Ethnicity, Percent=percent, Margin)

q44_eth

out <- q37_eth %>%
  rbind(q44_eth) %>%
  select(-4) %>%
  pivot_wider(#cols=2:3,
               names_from=Ethnicity,
               values_from=Percent) %>%
  gt() %>%
  fmt_percent(2:3, decimals=1) %>%
  tab_header("Social marginalization, by ethnicity")

%>%
  tab_source_note("Q37. Ethnic group treated unfairly by government") %>%
  tab_source_note("Q44. Religious group treated unfairly by government")

out

gtsave(out, "tables/social marginalization, by ethnicity.rtf")


frq(dat$Q45)
frq(dat$Q45_agree)

q45_eth <- svyrdat %>%
  group_by(ethnicity2) %>%
  summarise(percent=survey_mean(Q37_often, na.rm=T)) %>%
  na.omit() %>%
  mutate(Ethnicity = get_labels(dat$ethnicity2),
         Margin = 1.96* percent_se) %>%
  select(Ethnicity, Percent=percent, Margin)

q37_eth



q37_reg <- svyrdat %>%
  group_by(reg_name) %>%
  summarise(EthMarg= survey_mean(Q37_often, na.rm=T))

q37_reg


q37_maj <- svyrdat %>%
  group_by(maj_rel) %>%
  summarise(EthMarg= survey_mean(Q37_often, na.rm=T))

q37_maj


q37_bst <- svyrdat %>%
  group_by(boost_nat) %>%
  summarise(EthMarg= survey_mean(Q37_often, na.rm=T))

q37_bst


bst_eth <- svyrdat %>%
  group_by(boost_nat) %>%
  summarise(Albanian= survey_mean(albanian_bin, na.rm=T))

bst_eth


q37_bst_eth <- svyrdat %>%
  group_by(boost_nat, ethnicity2) %>%
  summarise(EthMarg= survey_mean(Q37_often, na.rm=T)) %>%
  na.omit() %>%
  mutate(Ethnicity=get_labels(dat$ethnicity2))

q37_bst_eth



veo_eth <- svyrdat %>%
  group_by(ethnicity2, Q37_often) %>%
  summarise(VEO= survey_mean(Q31ab_positive, na.rm=T)) %>%
  na.omit()
%>%
  mutate(Ethnicity=get_labels(dat$ethnicity2))

veo_eth


q37_VEO <- svyrdat %>%
  group_by(Q37_often) %>%
  summarise(VEO= survey_mean(Q31ab_positive, na.rm=T)) %>%
  na.omit()

q37_VEO


q37_ethno <- svyrdat %>%
  group_by(Q37_often) %>%
  summarise(ethno= survey_mean(Q32a_positive, na.rm=T)) %>%
  na.omit()

q37_ethno


q37_eth_ethno <- svyrdat %>%
  group_by(Q37_often,ethnicity2) %>%
  summarise(ethno= survey_mean(Q32a_positive, na.rm=T)) %>%
  na.omit()

q37_eth_ethno


q37_eth_ethno2 <- svyrdat %>%
  group_by(ethnicity2, Q37_often) %>%
  summarise(ethno= survey_mean(Q32a_positive, na.rm=T)) %>%
  na.omit()

q37_eth_ethno2



s1 <- svyglm(Q32a_positive ~ Q37_often,
             design=svydat)
summary(s1)

s2 <- svyglm(Q32a_positive ~ Q37_often:as.factor(ethnicity2),
             design=svydat)
summary(s2)


s3 <- svyglm(Q32a_positive ~ Q37_often + as.factor(ethnicity2) + Q37_often:as.factor(ethnicity2),
             design=svydat)
summary(s3)

alb <- dat %>%
  filter(albanian_bin==1)

lm(Q32a_positive ~ Q37_often,
   data=dat) %>% summary()


tes <- lmer(Q32a_positive ~ Q37_often + (Q37_often|ethnicity2),
            data=dat)

summary(tes)
ranef(tes)



tac1 <- svyglm(tactics_fac_bin ~ Q37_often,
               design=svydat)

summary(tac1)

tac2 <- svyglm(tactics_fac_bin ~ Q37_often + albanian_bin,
               design=svydat)

summary(tac2)



tac3 <- svyglm(tactics_fac_bin ~ Q37_often + albanian_bin + Q37_often:albanian_bin,
               design=svydat)

summary(tac3)


tacM <- lmer(tactics_fac_bin ~ Q37_often + (Q37_often|ethnicity2),
             data=dat)

summary(tacM)
ranef(tacM)


