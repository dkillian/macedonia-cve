
names(dat)

frq(dat$region)

reg <- dat %>%
  group_by(region) %>%
  summarise(margin = 1.96*std.error(brave),
            brave=mean(brave, na.rm=T)) %>%
  mutate(disag="region",
         category_name=get_labels(dat$reg_name)) %>%
  select(disag, category=region, category_name, brave, margin)

reg



frq(dat$boost)

boost <- dat %>%
  group_by(survey) %>%
  summarise(margin = 1.96*std.error(brave),
            brave=mean(brave, na.rm=T)) %>%
  mutate(disag="National and boost sample",
         category_name=get_labels(dat$survey)) %>%
  select(disag, category=survey, category_name, brave, margin)

boost



frq(dat$boost_nat)


boost_nat <- dat %>%
  group_by(boost_nat) %>%
  summarise(margin = 1.96*std.error(brave),
            brave=mean(brave, na.rm=T)) %>%
  mutate(disag="National with each boost sample",
         category=1:6) %>%
  select(disag, category, category_name=boost_nat, brave, margin)

boost_nat




frq(dat$boost_reg)

boost_reg <- dat %>%
  group_by(boost_reg) %>%
  summarise(margin = 1.96*std.error(brave),
            brave=mean(brave, na.rm=T)) %>%
  mutate(disag="each boost with region",
         category=1:13) %>%
  select(disag, category, category_name=boost_reg, brave, margin)

boost_reg



frq(dat$reg_boost)

reg_boost <- dat %>%
  group_by(reg_boost) %>%
  summarise(margin = 1.96*std.error(brave),
            brave=mean(brave, na.rm=T)) %>%
  mutate(disag="Region and boost sample",
         category=1:9) %>%
  select(disag, category, category_name=reg_boost, brave, margin)

reg_boost



frq(dat$maj_rel)

maj_rel <- dat %>%
  group_by(maj_rel) %>%
  summarise(margin = 1.96*std.error(brave),
            brave=mean(brave, na.rm=T)) %>%
  mutate(disag="Majority or minority religion",
         category_name=get_labels(dat$maj_rel)) %>%
  select(disag, category=maj_rel, category_name, brave, margin)

maj_rel


full <- do.call(rbind, list(reg, boost, boost_nat, boost_reg, reg_boost, maj_rel))

full
names(full)

full_gt <- full %>%
  gt(groupname_col = "disag")

full_gt

gtsave(full_gt, "tables/geographic cuts.rtf")


frq(dat$boost)

