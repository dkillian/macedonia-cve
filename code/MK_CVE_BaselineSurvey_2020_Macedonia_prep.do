**********************************************************************************************************		
	* 	Gustavo Castillo
	*	This do file: 
		*Relabels variables to make them easier to read
		*Collapses variables
		*Changes values to missing, where appropriate
	*	Dec 2020									    		
	*	Uses: .dta
	* 	Saves: MK_CVE_BaselineSurvey_2020_Macedonia_prep.dta
**************************************************************************************************************

*set working DIRECTORIES 	
global dirfolder "C:\Users\gustavo.castillo\Desktop\CVE Midline Survey Questionnaire for Bosnia and Herzegovina" 

*IMPORT data
clear
use "$dirfolder\Data\Midline\01_preprocessing\MK_CVE_BaselineSurvey_2020_raw_Macedonia_clean.dta", clear

*APPLY Codebook to clean variables

applyCodebook using "$dirfolder\_scripts\01_ingestion\codebooks\codebook.macedonia.xlsx", rename varlab recode

*DROP unnecessary variables
drop StartPoint1 UK0 S01x Q52a Q59 SupervGender1 Gender

*RENAME variable labels
label var Q1_opn "Who are you? (Other)"
label var Q12_opn "What is your current employment status? Specify"
label var Q22a1_opn "And who committed the violence? Other, Specify"
label var Q23a1_opn "And who committed the violence? Other, Specify"
label var Q33b1_opn "Where did you encounter these messages? ISIS - Specify"
label var Q33b2_opn "Where did you encounter these messages?AlQaeda-Specify"
label var Q33b3_opn "Where did you encounter these messages? Ethnonat-Specify"
label var Q33b4_opn "Where did you encounter these messages? Other - Specify"
label var Q33b5_opn "Where did you encounter these messages? Other - Specify"
label var Q36_opn "How would you characterize your ethnicity? Other, Specify"
label var Q38_opn "What religious group do you identify with? Other, Specify"
label var Q48_10_opn "In 2020, did anyone in your house exhibit symptoms of COVID?- Other, Specify"
label var Q48_11_opn "In 2020, did anyone in your house exhibit symptoms of COVID?- Other, Specify"

*RENAME variables that were not renamed in the codebook
rename T_51_4_S Q33a_4_opn
rename T_51_5_S Q33a_5_opn
rename T_58_10_S Q39_10_opn
rename Q53x Q53
rename Q53xx Q53_1
rename Q54x Q54_1
rename Useri enumerator_username
label var enumerator_username "Enumerator username"
rename SupVorUser supervisor_username
label var supervisor_username "Supervisor username"

*ORDER variables
order age_category, after (age)
order enumerator_gender, after (enumerator_username)

*COLLAPSE and label new variables

tab age_category
tabstat age_category, s(n min max)
gen age_category_grp=.
replace age_category_grp=1 if age_category>=1 & age_category<3
replace age_category_grp=2 if age_category>=3 & age_category<5
replace age_category_grp=3 if age_category>=5 & age_category<.
label var age_category_grp "Age Category (categorical)"
label def catgrp 1 "18-39" 2 "40-59" 3 "60+"
label val age_category_grp catgrp
order age_category_grp, after (age_category)
tab age_category_grp,m

gen female=gender>1
label var female "Female (Binary)"
label def femmale 1 "Female" 0 "Male"
label val female femmale
order female, after (gender)

gen ethnicity2=.
replace ethnicity2=1 if ethnicity==1 | ethnicity==2
replace ethnicity2=0 if ethnicity==3
label def macalb 0 "Other" 1 "Macedonian/Albanian"
label val ethnicity2 macalb
order ethnicity2, after (ethnicity)

gen macedonian_bin=.
replace macedonian_bin=1 if ethnicity==1
replace macedonian_bin=0 if ethnicity==2 | ethnicity==3
label var macedonian_bin "Macedonian (binary)"
label def maced 1 "Macedonian" 0 "Other"
label val macedonian_bin maced
order macedonian_bin, after (ethnicity2)

gen albanian_bin=.
replace albanian_bin=1 if ethnicity==2
replace albanian_bin=0 if ethnicity==1 | ethnicity==3
label var albanian_bin "Albanian (binary)"
label def albanianbin 1 "Albanian" 0 "Other"
label val albanian_bin albanianbin
order albanian_bin, after (macedonian_bin)

gen other_bin=.
replace other_bin=1 if ethnicity==3
replace other_bin=0 if ethnicity==1 | ethnicity==2
label var other_bin "Other (binary)"
label def otherbin 1 "Other" 0 "Macedonian/Albanian"
label val other_bin otherbin
order other_bin, after (albanian_bin)

gen religion_grp=.
replace religion_grp=1 if religion==1
replace religion_grp=2 if religion==2 | religion==3
replace religion_grp=3 if religion==6 | religion==7
label var religion_grp "Religion (categorical)"
label def relgrp 1 "Islam" 2 "Christian" 4 "Other"
label val religion_grp relgrp
order religion_grp, after (religion)

gen islam_bin=.
replace islam_bin=1 if religion_grp==1
replace islam_bin=0 if religion_grp==2 | religion_grp==3 | religion_grp==4
label var islam_bin "Islam (binary)"
label def islambin 1 "Islam" 0 "Other"
label val islam_bin islambin
order islam_bin, after (religion_grp)

gen catholic_bin=.
replace catholic_bin=1 if religion_grp==2
replace catholic_bin=0 if religion_grp==1 | religion_grp==3 | religion_grp==4
label var catholic_bin "Catholic (binary)"
label def cathbin 1 "Catholic" 0 "Other"
label val catholic_bin cathbin
order catholic_bin, after (islam_bin)

gen christian_bin=.
replace christian_bin=1 if religion_grp==3
replace christian_bin=0 if religion_grp==1 | religion_grp==2 | religion_grp==4
label var christian_bin "Christian (binary)"
label def christbin 1 "Christian" 0 "Other"
label val christian_bin christbin
order christian_bin, after (catholic_bin)

gen other_rel_bin=.
replace other_rel_bin=1 if religion_grp==4
replace other_rel_bin=0 if religion_grp==1 | religion_grp==2 | religion_grp==3
label var other_rel_bin "Other (binary)"
label def otherrelbin 1 "Other" 0 "Islam/Catholic/Christian Orthodox"
label val other_rel_bin otherrelbin
order other_rel_bin, after (christian_bin)

gen Q2a_grp=.
replace Q2a_grp=1 if Q2a==1 | Q2a==2
replace Q2a_grp=2 if Q2a==3 | Q2a==4
replace Q2a_grp=3 if Q2a==5 
label var Q2a_grp "How many of these groups do you belong to, in total? (categorical)"
label def howmany 1 "1-2" 2 "3-4" 3 "5+"
label val Q2a_grp howmany
order Q2a_grp, after (Q2a)

gen Q3_1_accept=.
replace Q3_1_accept=0 if Q3_1==1 | Q3_1==2
replace Q3_1_accept=1 if Q3_1==3 | Q3_1==4
label var Q3_1_accept "How accept would you be of ppl of diff religion? - husband or wife (binary)"
label def accepting 0 "Not accepting" 1 "Accepting"
label val Q3_1_accept accepting
order Q3_1_accept, after (Q3_1)

gen Q3_2_accept=.
replace Q3_2_accept=0 if Q3_2==1 | Q3_2==2
replace Q3_2_accept=1 if Q3_2==3 | Q3_2==4
label var Q3_2_accept "How accepting would you be of ppl. of diff. religion?-Relative by marriage(binary)"
label val Q3_2_accept accepting
order Q3_2_accept, after (Q3_2)

gen Q3_3_accept=.
replace Q3_3_accept=0 if Q3_3==1 | Q3_3==2
replace Q3_3_accept=1 if Q3_3==3 | Q3_3==4
label var Q3_3_accept "How accepting would you be of ppl. of diff. religion? - Friend (binary)"
label val Q3_3_accept accepting
order Q3_3_accept, after (Q3_3)

gen Q3_4_accept=.
replace Q3_4_accept=0 if Q3_4==1 | Q3_4==2
replace Q3_4_accept=1 if Q3_4==3 | Q3_4==4
label var Q3_4_accept "How accepting would you be of ppl. of diff. religion? - Neighbor (binary)"
label val Q3_4_accept accepting
order Q3_4_accept, after (Q3_4)

gen Q3_5_accept=.
replace Q3_5_accept=0 if Q3_5==1 | Q3_5==2
replace Q3_5_accept=1 if Q3_5==3 | Q3_5==4
label var Q3_5_accept "How accepting would you be of ppl. of diff. religion? - Colleague (binary)"
label val Q3_5_accept accepting
order Q3_5_accept, after (Q3_5)

gen Q3_6_accept=.
replace Q3_6_accept=0 if Q3_6==1 | Q3_6==2
replace Q3_6_accept=1 if Q3_6==3 | Q3_6==4
label var Q3_6_accept "How accepting would you be of ppl. of diff. religion? - Shop owner (binary)"
label val Q3_6_accept accepting
order Q3_6_accept, after (Q3_6)

gen Q3_7_accept=.
replace Q3_7_accept=0 if Q3_7==1 | Q3_7==2
replace Q3_7_accept=1 if Q3_7==3 | Q3_7==4
label var Q3_7_accept "How accepting would you be of ppl. of diff. religion? - Govt. official(binary)"
label val Q3_7_accept accepting
order Q3_7_accept, after (Q3_7)

gen Q4_1_trust=.
replace Q4_1_trust=0 if Q4_1==1 | Q4_1==2
replace Q4_1_trust=1 if Q4_1==3 | Q4_1==4
label var Q4_1_trust "How trustworthy are? - People from your religious group (binary)"
label def trustworth 0 "Not trustworthy" 1 "Trustworthy"
label val Q4_1_trust trustworth
order Q4_1_trust, after (Q4_1)

gen Q4_2_trust=.
replace Q4_2_trust=0 if Q4_2==1 | Q4_2==2
replace Q4_2_trust=1 if Q4_2==3 | Q4_2==4
label var Q4_2_trust "How trustworthy are? - People from a different religious group (binary)"
label val Q4_2_trust trustworth
order Q4_2_trust, after (Q4_2)

gen Q4_3_trust=.
replace Q4_3_trust=0 if Q4_3==1 | Q4_3==2
replace Q4_3_trust=1 if Q4_3==3 | Q4_3==4
label var Q4_3_trust "How trustworthy are? - People from your ethnic group (binary)"
label val Q4_3_trust trustworth
order Q4_3_trust, after (Q4_3)

gen Q4_4_trust=.
replace Q4_4_trust=0 if Q4_4==1 | Q4_4==2
replace Q4_4_trust=1 if Q4_4==3 | Q4_4==4
label var Q4_4_trust "How trustworthy are? - People from a different ethnic group (binary)"
label val Q4_4_trust trustworth
order Q4_4_trust, after (Q4_4)

gen Q5_1_agree=.
replace Q5_1_agree=0 if Q5_1==1 | Q5_1==2 | Q5_1==3
replace Q5_1_agree=1 if Q5_1==4 | Q5_1==5
label var Q5_1_agree "It's important for me to maintain cultural traditions (binary)"
label def agreedoes 0 "Does not agree" 1 "Agree"
label val Q5_1_agree agreedoes
order Q5_1_agree, after (Q5_1)

gen Q5_2_agree=.
replace Q5_2_agree=0 if Q5_2==1 | Q5_2==2 | Q5_2==3
replace Q5_2_agree=1 if Q5_2==4 | Q5_2==5
label var Q5_2_agree "I am familiar w my cult trad/beliefs/values (binary)"
label val Q5_2_agree agreedoes
order Q5_2_agree, after (Q5_2)

gen Q5_3_agree=.
replace Q5_3_agree=0 if Q5_3==1 | Q5_3==2 | Q5_3==3
replace Q5_3_agree=1 if Q5_3==4 | Q5_3==5
label var Q5_3_agree "My cultural id guides the way I live my life (binary)"
label val Q5_3_agree agreedoes
order Q5_3_agree, after (Q5_3)

gen Q6_will=.
replace Q6_will=0 if Q6==1 | Q6==2
replace Q6_will=1 if Q6==3 | Q6==4
label var Q6_will "How willing are people around here to help their neighbors? (binary)"
label def willing 0 "Not willing" 1 "Willing"
label val Q6_will willing
order Q6_will, after (Q6)

gen Q7_1_agree=.
replace Q7_1_agree=0 if Q7_1==1 | Q7_1==2 | Q7_1==3
replace Q7_1_agree=1 if Q7_1==4 | Q7_1==5
label var Q7_1_agree "In general, I trust ppl from other communities (binary)"
label val Q7_1_agree agreedoes
order Q7_1_agree, after (Q7_1)

gen Q7_2_agree=.
replace Q7_2_agree=0 if Q7_2==1 | Q7_2==2 | Q7_2==3
replace Q7_2_agree=1 if Q7_2==4 | Q7_2==5
label var Q7_2_agree "I feel supported by ppl from other communities (binary)"
label val Q7_2_agree agreedoes
order Q7_2_agree, after (Q7_2)

gen Q7_3_agree=.
replace Q7_3_agree=0 if Q7_3==1 | Q7_3==2 | Q7_3==3
replace Q7_3_agree=1 if Q7_3==4 | Q7_3==5
label var Q7_3_agree "I regularly engage in conv with ppl of multiple relig (binary)"
label val Q7_3_agree agreedoes
order Q7_3_agree, after (Q7_3)

gen Q8_1_trust=.
replace Q8_1_trust=0 if Q8_1==1 | Q8_1==2
replace Q8_1_trust=1 if Q8_1==3 | Q8_1==4
label var Q8_1_trust "How trustworthy is? - Prime Minister of N. Macedonia (binary)"
label val Q8_1_trust trustworth
order Q8_1_trust, after (Q8_1)

gen Q8_2_trust=.
replace Q8_2_trust=0 if Q8_2==1 | Q8_2==2
replace Q8_2_trust=1 if Q8_2==3 | Q8_2==4
label var Q8_2_trust "How trustworthy is? - Leaders of your fav pol party (binary)"
label val Q8_2_trust trustworth
order Q8_2_trust, after (Q8_2)

gen Q8_3_trust=.
replace Q8_3_trust=0 if Q8_3==1 | Q8_3==2
replace Q8_3_trust=1 if Q8_3==3 | Q8_3==4
label var Q8_3_trust "How trustworthy are? - Police (binary)"
label val Q8_3_trust trustworth
order Q8_3_trust, after (Q8_3)

gen Q8_4_trust=.
replace Q8_4_trust=0 if Q8_4==1 | Q8_4==2
replace Q8_4_trust=1 if Q8_4==3 | Q8_4==4
label var Q8_4_trust "How trustworthy are? - Local religious officials (binary)"
label val Q8_4_trust trustworth
order Q8_4_trust, after (Q8_4)

gen Q8_5_trust=.
replace Q8_5_trust=0 if Q8_5==1 | Q8_5==2
replace Q8_5_trust=1 if Q8_5==3 | Q8_5==4
label var Q8_5_trust "How trustworthy are? - Highest religious authority (binary)"
label val Q8_5_trust trustworth
order Q8_5_trust, after (Q8_5)

gen Q9_1_agree=.
replace Q9_1_agree=0 if Q9_1==1 | Q9_1==2 | Q9_1==3
replace Q9_1_agree=1 if Q9_1==4 | Q9_1==5
label var Q9_1_agree "Pol parties in N Macedonia increase divisions in society (binary)"
label val Q9_1_agree agreedoes
order Q9_1_agree, after (Q9_1)

gen Q9_2_agree=.
replace Q9_2_agree=0 if Q9_2==1 | Q9_2==2 | Q9_2==3
replace Q9_2_agree=1 if Q9_2==4 | Q9_2==5
label var Q9_2_agree "Pol parties plays a role in hiring dec in public sector (binary)"
label val Q9_2_agree agreedoes
order Q9_2_agree, after (Q9_2)

gen Q10_1_agree=.
replace Q10_1_agree=0 if Q10_1==1 | Q10_1==2 | Q10_1==3
replace Q10_1_agree=1 if Q10_1==4 | Q10_1==5
label var Q10_1_agree "Agree? - Most of what I do in my day is meaningful (binary)"
label val Q10_1_agree agreedoes
order Q10_1_agree, after (Q10_1)

gen Q10_2_agree=.
replace Q10_2_agree=0 if Q10_2==1 | Q10_2==2 | Q10_2==3
replace Q10_2_agree=1 if Q10_2==4 | Q10_2==5
label var Q10_2_agree "Agree? My opinions are respected by comm lead (binary)"
label val Q10_2_agree agreedoes
order Q10_2_agree, after (Q10_2)

gen Q10_3_agree=.
replace Q10_3_agree=0 if Q10_3==1 | Q10_3==2 | Q10_3==3
replace Q10_3_agree=1 if Q10_3==4 | Q10_3==5
label var Q10_3_agree "Agree? I am able to make a pos. diff in my fam/commu (binary)"
label val Q10_3_agree agreedoes
order Q10_3_agree, after (Q10_3)

gen Q11_1_agree=.
replace Q11_1_agree=0 if Q11_1==1 | Q11_1==2 | Q11_1==3
replace Q11_1_agree=1 if Q11_1==4 | Q11_1==5
label var Q11_1_agree "Agree/disagree? Women should be free to work outside the home (binary)"
label val Q11_1_agree agreedoes
order Q11_1_agree, after (Q11_1)

gen Q11_2_agree=.
replace Q11_2_agree=0 if Q11_2==1 | Q11_2==2 | Q11_2==3
replace Q11_2_agree=1 if Q11_2==4 | Q11_2==5
label var Q11_2_agree "Agree/disagree? Women should have same chance of being elected as men(binary)"
label val Q11_2_agree agreedoes
order Q11_2_agree, after (Q11_2)

gen Q12_bin_uni_student=.
replace Q12_bin_uni_student=1 if Q12==8
replace Q12_bin_uni_student=0 if Q12==1 | Q12==2 | Q12==3 | Q12==4 | Q12==5 | Q12==6 | Q12==7 | Q12==9 | Q12==10
label def student 0 "Not student" 1 "Student"
label val Q12_bin_uni_student student
order Q12_bin_uni_student, after (Q12)

gen Q12_bin_unemployed=.
replace Q12_bin_unemployed=1 if Q12==2
replace Q12_bin_unemployed=0 if Q12==1 | Q12==3 | Q12==4 | Q12==5 | Q12==6 | Q12==7 | Q12==8 | Q12==9 | Q12==10
label def unemployed 0 "Not unemployed" 1 "Unemployed"
label val Q12_bin_unemployed unemployed
order Q12_bin_unemployed, after (Q12_bin_uni_student)

gen Q16_1_optimistic=.
replace Q16_1_optimistic=0 if Q16==1 | Q16==2
replace Q16_1_optimistic=1 if Q16==3 | Q16==4
label var Q16_1_optimistic "How optimistic are you that you can achieve a better future? (binary)"
label def optimism 0 "Pessimistic" 1 "Optimistic"
label val Q16_1_optimistic optimism
order Q16_1_optimistic, after (Q16)

gen Q17_1_satisfied=.
replace Q17_1_satisfied=0 if Q17_1==1 | Q17_1==2
replace Q17_1_satisfied=1 if Q17_1==3 | Q17_1==4
label var Q17_1_satisfied "Satisfied/dissatisfied? Education/schools (binary)"
label def satisfied 0 "Dissatisfied" 1 "Satisfied"
label val Q17_1_satisfied satisfied
order Q17_1_satisfied, after (Q17_1)

gen Q17_2_satisfied=.
replace Q17_2_satisfied=0 if Q17_2==1 | Q17_2==2
replace Q17_2_satisfied=1 if Q17_2==3 | Q17_2==4
label var Q17_2_satisfied "Satisfied/dissatisfied? Medical care/clinics and hospitals (binary)"
label val Q17_2_satisfied satisfied
order Q17_2_satisfied, after (Q17_2)

gen Q17_3_satisfied=.
replace Q17_3_satisfied=0 if Q17_3==1 | Q17_3==2
replace Q17_3_satisfied=1 if Q17_3==3 | Q17_3==4
label var Q17_3_satisfied "Satisfied/dissatisfied? Public safety/police (binary)"
label val Q17_3_satisfied satisfied
order Q17_3_satisfied, after (Q17_3)

gen Q17_4_satisfied=.
replace Q17_4_satisfied=0 if Q17_4==1 | Q17_4==2
replace Q17_4_satisfied=1 if Q17_4==3 | Q17_4==4
label var Q17_4_satisfied "Satisfied/dissatisfied? Water (binary)"
label val Q17_4_satisfied satisfied
order Q17_4_satisfied, after (Q17_4)

gen Q17_5_satisfied=.
replace Q17_5_satisfied=0 if Q17_5==1 | Q17_5==2
replace Q17_5_satisfied=1 if Q17_5==3 | Q17_5==4
label var Q17_5_satisfied "Satisfied/dissatisfied? Electricity (binary)"
label val Q17_5_satisfied satisfied
order Q17_5_satisfied, after (Q17_5)

gen Q18_1_agree=.
replace Q18_1_agree=0 if Q18_1==1 | Q18_1==2 | Q18_1==3
replace Q18_1_agree=1 if Q18_1==4 | Q18_1==5
label var Q18_1_agree "Agree? N Macedonias cult make it a good fit for EU (binary)"
label val Q18_1_agree agreedoes
order Q18_1_agree, after (Q18_1)

gen Q18_2_agree=.
replace Q18_2_agree=0 if Q18_2==1 | Q18_2==2 | Q18_2==3
replace Q18_2_agree=1 if Q18_2==4 | Q18_2==5
label var Q18_2_agree "Agree? EU accession offers opp to improve stand of liv in N Mac.(binary)"
label val Q18_2_agree agreedoes
order Q18_2_agree, after (Q18_2)

gen Q18_3_agree=.
replace Q18_3_agree=0 if Q18_3==1 | Q18_3==2 | Q18_3==3
replace Q18_3_agree=1 if Q18_3==4 | Q18_3==5
label var Q18_3_agree "Agree? EU accession offers opp to reduce corrupt in NM (binary)"
label val Q18_3_agree agreedoes
order Q18_3_agree, after (Q18_3)

gen Q20_1_agree=.
replace Q20_1_agree=0 if Q20_1==1 | Q20_1==2 | Q20_1==3
replace Q20_1_agree=1 if Q20_1==4 | Q20_1==5
label var Q20_1_agree "I am willing to speak out against viol in my com (binary)"
label val Q20_1_agree agreedoes
order Q20_1_agree, after (Q20_1)

gen Q20_2_agree=.
replace Q20_2_agree=0 if Q20_2==1 | Q20_2==2 | Q20_2==3
replace Q20_2_agree=1 if Q20_2==4 | Q20_2==5
label var Q20_2_agree "I am willing to challenge viol behavior of others in my com (binary)"
label val Q20_2_agree agreedoes
order Q20_2_agree, after (Q20_2)

gen Q21_1_agree=.
replace Q21_1_agree=0 if Q21_1==1 | Q21_1==2 | Q21_1==3
replace Q21_1_agree=1 if Q21_1==4 | Q21_1==5
label var Q21_1_agree "Agree? Being violent helps me earn the respect of others (binary)"
label val Q21_1_agree agreedoes
order Q21_1_agree, after (Q21_1)

gen Q21_2_agree=.
replace Q21_2_agree=0 if Q21_2==1 | Q21_2==2 | Q21_2==3
replace Q21_2_agree=1 if Q21_2==4 | Q21_2==5
label var Q21_2_agree "Agree? Being violent helps show how strong I am (binary)"
label val Q21_2_agree agreedoes
order Q21_2_agree, after (Q21_2)

gen Q21_3_agree=.
replace Q21_3_agree=0 if Q21_3==1 | Q21_3==2 | Q21_3==3
replace Q21_3_agree=1 if Q21_3==4 | Q21_3==5
label var Q21_3_agree "Agree? My community accepts that young people may use violence to solve problems (binary)"
label val Q21_3_agree agreedoes
order Q21_3_agree, after (Q21_3)

gen Q24_1_agree=.
replace Q24_1_agree=0 if Q24==1 | Q24==2 | Q24==3
replace Q24_1_agree=1 if Q24==4 | Q24==5
label var Q24_1_agree "Agree/dis? I personally exp disc against ppl because of race/ethn/relig (binary)"
label val Q24_1_agree agreedoes
order Q24_1_agree, after (Q24)

gen Q25_bin=.
replace Q25_bin=0 if Q25==1 | Q25==2 | Q25==3
replace Q25_bin=1 if Q25==4 | Q25==5
label var Q25_bin "How often do law enforce agencies treat ppl equally under law? (binary)"
label def frequencyneveral 0 "Never" 1 "Often/Always"
label val Q25_bin frequencyneveral
order Q25_bin, after (Q25)

gen Q26_1_agree=.
replace Q26_1_agree=0 if Q26_1==1 | Q26_1==2 | Q26_1==3
replace Q26_1_agree=1 if Q26_1==4 | Q26_1==5
label var Q26_1_agree "Agree/disagree? I trust authorities/law enforcement agencies (binary)"
label val Q26_1_agree agreedoes
order Q26_1_agree, after (Q26_1)

gen Q26_2_agree=.
replace Q26_2_agree=0 if Q26_2==1 | Q26_2==2 | Q26_2==3
replace Q26_2_agree=1 if Q26_2==4 | Q26_2==5
label var Q26_2_agree "Agree/disagree? I feel confident when dealing with govt/authorities (binary)"
label val Q26_2_agree agreedoes
order Q26_2_agree, after (Q26_2)

gen Q26_3_agree=.
replace Q26_3_agree=0 if Q26_3==1 | Q26_3==2 | Q26_3==3
replace Q26_3_agree=1 if Q26_3==4 | Q26_3==5
label var Q26_3_agree "Agree/disagree? I feel that my voice is heard when dealing  with govt/authorities (binary)"
label val Q26_3_agree agreedoes
order Q26_3_agree, after (Q26_3)

gen Q28_1_afraid=.
replace Q28_1_afraid=0 if Q28==1 | Q28==2
replace Q28_1_afraid=1 if Q28==3 | Q28==4
label var Q28_1_afraid "Are you afraid of a VE incident happening in your comm?(binary)"
label def afraid 0 "Not afraid" 1 "Afraid"
label val Q28_1_afraid afraid
order Q28_1_afraid, after (Q28)

gen Q31a_positive=.
replace Q31a_positive=0 if Q31a==1 | Q31a==2
replace Q31a_positive=1 if Q31a==3 | Q31a==4
label var Q31a_positive "What is your opinion of the Islamic State?(binary)"
label def positive 0 "Negative" 1 "Positive"
label val Q31a_positive positive
order Q31a_positive, after (Q31a)

gen Q31b_positive=.
replace Q31b_positive=0 if Q31b==1 | Q31b==2
replace Q31b_positive=1 if Q31b==3 | Q31b==4
label var Q31b_positive "What is your opinion of Al Qaeda?(binary)"
label val Q31b_positive positive
order Q31b_positive, after (Q31b)

gen Q32a_positive=.
replace Q32a_positive=0 if Q32a==1 | Q32a==2
replace Q32a_positive=1 if Q32a==3 | Q32a==4
label var Q32a_positive "What is your opinion of Russia’s support to separatists in Ukraine?(binary)"
label val Q32a_positive positive
order Q32a_positive, after (Q32a)

gen Q34_positive=.
replace Q34_positive=0 if Q34==1 | Q34==2
replace Q34_positive=1 if Q34==3 | Q34==4
label var Q34_positive "How would you evaluate the work of govt secur agen in deterring VE?(binary)"
label val Q34_positive positive
order Q34_positive, after (Q34)

gen Q35_bin=.
replace Q35_bin=0 if Q35==1 | Q35==2 | Q35==3
replace Q35_bin=1 if Q35==4 | Q35==5
label var Q35_bin "Agree/disagree? Govt should supp reintegrat of former combatants into local com (binary)"
label val Q35_bin agreedoes
order Q35_bin, after (Q35)

gen Q43_1_agree=.
replace Q43_1_agree=0 if Q43_1==1 | Q43_1==2 | Q43_1==3
replace Q43_1_agree=1 if Q43_1==4 | Q43_1==5
label var Q43_1_agree "Agree/Disagree?Officials of my rel should hold design posit. in govt(binary)"
label val Q43_1_agree agreedoes
order Q43_1_agree, after (Q43_1)

gen Q43_2_agree=.
replace Q43_2_agree=0 if Q43_2==1 | Q43_2==2 | Q43_1==3
replace Q43_2_agree=1 if Q43_2==4 | Q43_2==5
label var Q43_2_agree "Agree/Disagree?My relig is compatible with democracy (binary)"
label val Q43_2_agree agreedoes
order Q43_2_agree, after (Q43_2)

gen Q45_bin=.
replace Q45_bin=0 if Q45==1 | Q45==2 | Q45==3
replace Q45_bin=1 if Q45==4 | Q45==5
label var Q45_bin "To what extent do you agree w: Islam is under threat(binary)"
label val Q45_bin agreedoes
order Q45_bin, after (Q45)

gen Q47a_agree=.
replace Q47a_agree=0 if Q47a==1 | Q47a==2 | Q47a==3
replace Q47a_agree=1 if Q47a==4 | Q47a==5
label var Q47a_agree "Agree/dis? Officials of my religion should help settle civil disputes(binary)"
label val Q47a_agree agreedoes
order Q47a_agree, after (Q47a)

gen Q47b_agree=.
replace Q47b_agree=0 if Q47b==1 | Q47b==2 | Q47b==3
replace Q47b_agree=1 if Q47b==4 | Q47b==5
label var Q47b_agree "Agree/dis? ISIS believes relig officials should help settle civil disputes(binary)"
label val Q47b_agree agreedoes
order Q47b_agree, after (Q47b)

gen Q48_O1_s1=.
replace Q48_O1_s1=1 if Q48_O1==1 | Q48_O2==1 | Q48_O3==1 | Q48_O4==1 | Q48_O5==1 | Q48_O6==1 | Q48_O7==1 | Q48_O8==1 | Q48_O9==1 | Q48_O10==1 | Q48_O11==1 | Q48_O12==1 | Q48_O13==1 | Q48_O14==1
label var Q48_O1_s1 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Fever"
label def fever  1 "Fever"
label val Q48_O1_s1 fever
order Q48_O1_s1, after (Q48_O1)

gen Q48_O2_s2=.
replace Q48_O2_s2=1 if Q48_O1==2 |Q48_O2==2 | Q48_O3==2 | Q48_O4==2 | Q48_O5==2 | Q48_O6==2 | Q48_O7==2 | Q48_O8==2 | Q48_O9==2 | Q48_O10==2 | Q48_O11==2 | Q48_O12==2 | Q48_O13==2 | Q48_O14==2
label var Q48_O2_s2 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Fatigue"
label def fatigue 1 "Fatigue"
label val Q48_O2_s2 fatigue
order Q48_O2_s2, after (Q48_O2)

gen Q48_O3_s3=.
replace Q48_O3_s3=1 if Q48_O1==3 |Q48_O2==3 | Q48_O3==3 | Q48_O4==3 | Q48_O5==3 | Q48_O6==3 | Q48_O7==3 | Q48_O8==3 | Q48_O9==3 | Q48_O10==3 | Q48_O11==3 | Q48_O12==3 | Q48_O13==3 | Q48_O14==3
label var Q48_O3_s3 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Dry cough"
label def drycough 1 "Dry cough"
label val Q48_O3_s3 drycough
order Q48_O3_s3, after (Q48_O3)

gen Q48_O4_s4=.
replace Q48_O4_s4=1 if Q48_O1==4 |Q48_O2==4 | Q48_O3==4 | Q48_O4==4 | Q48_O5==4 | Q48_O6==4 | Q48_O7==4 | Q48_O8==4 | Q48_O9==4 | Q48_O10==4 | Q48_O11==4 | Q48_O12==4 | Q48_O13==4 | Q48_O14==4
label var Q48_O4_s4 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Difficulty breathing"
label def diffbr 1 "Difficulty breathing"
label val Q48_O4_s4 diffbr
order Q48_O4_s4, after (Q48_O4)

gen Q48_O5_s5=.
replace Q48_O5_s5=1 if Q48_O1==5 |Q48_O2==5 | Q48_O3==5 | Q48_O4==5 | Q48_O5==5 | Q48_O6==5 | Q48_O7==5 | Q48_O8==5 | Q48_O9==5 | Q48_O10==5 | Q48_O11==5 | Q48_O12==5 | Q48_O13==5 | Q48_O14==5
label var Q48_O5_s5 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Sore throat"
label def sore 1 "Sore throat"
label val Q48_O5_s5 sore
order Q48_O5_s5, after (Q48_O5)

gen Q48_O6_s6=.
replace Q48_O6_s6=1 if Q48_O1==6 |Q48_O2==6 | Q48_O3==6 | Q48_O4==6 | Q48_O5==6 | Q48_O6==6 | Q48_O7==6 | Q48_O8==6 | Q48_O9==6 | Q48_O10==6 | Q48_O11==6 | Q48_O12==6 | Q48_O13==6 | Q48_O14==6
label var Q48_O6_s6 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Headache"
label def headache 1 "Headache"
label val Q48_O6_s6 headache
order Q48_O6_s6, after (Q48_O6)

gen Q48_O7_s7=.
replace Q48_O7_s7=1 if Q48_O1==7 |Q48_O2==7 | Q48_O3==7 | Q48_O4==7 | Q48_O5==7 | Q48_O6==7 | Q48_O7==7 | Q48_O8==7 | Q48_O9==7 | Q48_O10==7 | Q48_O11==7 | Q48_O12==7 | Q48_O13==7 | Q48_O14==7
label var Q48_O7_s7 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Muscle ache"
label def muscle 1 "Muscle ache"
label val Q48_O7_s7 muscle
order Q48_O7_s7, after (Q48_O7)

gen Q48_O8_s8=.
replace Q48_O8_s8=1 if Q48_O1==8 |Q48_O2==8 | Q48_O3==8 | Q48_O4==8 | Q48_O5==8 | Q48_O6==8 | Q48_O7==8 | Q48_O8==8 | Q48_O9==8 | Q48_O10==8 | Q48_O11==8 | Q48_O12==8 | Q48_O13==8 | Q48_O14==8
label var Q48_O8_s8 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Loss of taste"
label def taste 1 "Loss of taste"
label val Q48_O8_s8 taste
order Q48_O8_s8, after (Q48_O8)

gen Q48_O9_s9=.
replace Q48_O9_s9=1 if Q48_O1==9 |Q48_O2==9 | Q48_O3==9 | Q48_O4==9 | Q48_O5==9 | Q48_O6==9 | Q48_O7==9 | Q48_O8==9 | Q48_O9==9 | Q48_O10==9 | Q48_O11==9 | Q48_O12==9 | Q48_O13==9 | Q48_O14==9
label var Q48_O9_s9 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Loss of smell"
label def smell 1 "Loss of smell"
label val Q48_O9_s9 smell
order Q48_O9_s9, after (Q48_O9)

gen Q48_13_s13=.
replace Q48_13_s13=1 if Q48_O1==13 |Q48_O2==13 | Q48_O3==13 | Q48_O4==13 | Q48_O5==13| Q48_O6==13 | Q48_O7==13 | Q48_O8==13 | Q48_O9==13 | Q48_O10==13 | Q48_O11==13 | Q48_O12==13 | Q48_O13==13 | Q48_O14==13
label var Q48_13_s13 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Joints pain"
label def joints 1 "Joints pain"
label val Q48_13_s13 joints
order Q48_13_s13, after (Q48_O13)

gen Q48_14_s14=.
replace Q48_14_s14=1 if Q48_O1==14 |Q48_O2==14 | Q48_O3==14 | Q48_O4==14 | Q48_O5==14| Q48_O6==14 | Q48_O7==14 | Q48_O8==14 | Q48_O9==14 | Q48_O10==14 | Q48_O11==14 | Q48_O12==14 | Q48_O13==14 | Q48_O14==14
label var Q48_14_s14 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - High blood preassure"
label def hbp 1 "High blood preassure"
label val Q48_14_s14 hbp
order Q48_14_s14, after (Q48_O14)

gen Q48_15_s15=.
replace Q48_15_s15=1 if Q48_O1==15 |Q48_O2==15 | Q48_O3==15 | Q48_O4==15 | Q48_O5==15| Q48_O6==15 | Q48_O7==15 | Q48_O8==15 | Q48_O9==15 | Q48_O10==15 | Q48_O11==15 | Q48_O12==15 | Q48_O13==15 | Q48_O14==15
label var Q48_15_s15 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - High temperature"
label def temperature 1 "High temperature"
label val Q48_15_s15 temperature
order Q48_15_s15, after (Q48_14_s14)

gen Q48_16_s16=.
replace Q48_16_s16=1 if Q48_O1==16 |Q48_O2==16 | Q48_O3==16 | Q48_O4==16 | Q48_O5==16| Q48_O6==16 | Q48_O7==16 | Q48_O8==16 | Q48_O9==16 | Q48_O10==16 | Q48_O11==16 | Q48_O12==16 | Q48_O13==16 | Q48_O14==16
label var Q48_16_s16 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Coughing/sneezing"
label def coughing 1 "Coughing/sneezing"
label val Q48_16_s16 coughing
order Q48_16_s16, after (Q48_15_s15)

gen Q48_17_s17=.
replace Q48_17_s17=1 if Q48_O1==17 |Q48_O2==17 | Q48_O3==17 | Q48_O4==17 | Q48_O5==17| Q48_O6==17 | Q48_O7==17 | Q48_O8==17 | Q48_O9==17 | Q48_O10==17 | Q48_O11==17 | Q48_O12==17 | Q48_O13==17 | Q48_O14==17
label var Q48_17_s17 "In 2020, did anyone in your household exhibit symptoms of COVID-19? - Stomach problems"
label def stomach 1 "Stomach problems"
label val Q48_17_s17 stomach
order Q48_17_s17, after (Q48_16_s16)

gen Q50_1_satisfied=.
replace Q50_1_satisfied=0 if Q50_1==1 | Q50_1==2
replace Q50_1_satisfied=1 if Q50_1==3 | Q50_1==4
label var Q50_1_satisfied "How satisfied are you with your central govt response to COVID?(binary)"
label val Q50_1_satisfied satisfied
order Q50_1_satisfied, after (Q50_1)

gen Q50_2_satisfied=.
replace Q50_2_satisfied=0 if Q50_2==1 | Q50_2==2
replace Q50_2_satisfied=1 if Q50_2==3 | Q50_2==4
label var Q50_2_satisfied "How satisfied are you with your local govt response to COVID?(binary)"
label val Q50_2_satisfied satisfied
order Q50_2_satisfied, after (Q50_2)

gen Q51_1_likely=.
replace Q51_1_likely=0 if Q51_1==1 | Q51_1==2
replace Q51_1_likely=1 if Q51_1==3 | Q51_1==4
label var Q51_1_likely "How likely? COVID-19 was created through the natural process of evolution(binary)"
label def likely 0 "Not likely" 1 "Likely"
label val Q51_1_likely satisfied
order Q51_1_likely, after (Q51_1)

gen Q51_2_likely=.
replace Q51_2_likely=0 if Q51_2==1 | Q51_2==2
replace Q51_2_likely=1 if Q51_2==3 | Q51_2==4
label var Q51_2_likely "How likely? COVID was transmitted from bats to humans through normal interactions (binary)"
label val Q51_2_likely satisfied
order Q51_2_likely, after (Q51_2)

gen Q51_3_likely=.
replace Q51_3_likely=0 if Q51_3==1 | Q51_3==2
replace Q51_3_likely=1 if Q51_3==3 | Q51_3==4
label var Q51_3_likely "How likely? COVID was accident released from lab where scientists were studying virus(binary)"
label val Q51_3_likely satisfied
order Q51_3_likely, after (Q51_3)

gen Q51_4_likely=.
replace Q51_4_likely=0 if Q51_4==1 | Q51_4==2
replace Q51_4_likely=1 if Q51_4==3 | Q51_4==4
label var Q51_4_likely "How likely? COVID-19 was deliberately released by a foreign govt(binary)"
label val Q51_4_likely satisfied
order Q51_4_likely, after (Q51_4)

gen Q51_5_likely=.
replace Q51_5_likely=0 if Q51_5==1 | Q51_5==2
replace Q51_5_likely=1 if Q51_5==3 | Q51_5==4
label var Q51_5_likely "How likely? COVID-19 was deliberately released by a foreign figure/org(binary)"
label val Q51_5_likely satisfied
order Q51_5_likely, after (Q51_5)

gen Q51_6_likely=.
replace Q51_6_likely=0 if Q51_6==1 | Q51_6==2
replace Q51_6_likely=1 if Q51_6==3 | Q51_6==4
label var Q51_6_likely "How likely? COVID-19 was sent by God as a consequence of our sins(binary)"
label val Q51_6_likely satisfied
order Q51_6_likely, after (Q51_6)
 
* Redefine value labels

*age
gen age_sq=age^2
order age_sq, after (age)

*Q1
gen Q1_religion=.
replace Q1_religion=1 if Q1==5
replace Q1_religion=0 if Q1==1 | Q1==2 | Q1==3 | Q1==4 | Q1==6 | Q1==7 | Q1==8 | Q1==9
label def idrel 0 "Other" 1 "Religion-related response"
label val Q1_religion idrel
order Q1_religion, after (Q1_opn)

*Q14
label val Q14 .
replace Q14=-2 if Q14==1
replace Q14=-1 if Q14==2
replace Q14=0 if Q14==3
replace Q14=1 if Q14==4
replace Q14=2 if Q14==5
label def worsen -2 "Much worse" -1 "Somewhat worse" 0 "About the same" 1 "Somewhat better" 2 "Much better"
label val Q14 worsen

gen Q14_bin=.
replace Q14_bin=1 if Q14==1 | Q14==2
replace Q14_bin=0 if Q14==-2 | Q14==-1 | Q14==0
label def worsebetter 0 "Worse" 1 "Better"
label val Q14_bin worsebetter
order Q14_bin, after (Q14) 

*Q15
label val Q15 .
replace Q15=-2 if Q15==1
replace Q15=-1 if Q15==2
replace Q15=0 if Q15==3
replace Q15=1 if Q15==4
replace Q15=2 if Q15==5
label val Q15 worsen

gen Q15_bin=.
replace Q15_bin=1 if Q15==1 | Q15==2
replace Q15_bin=0 if Q15==-2 | Q15==-1 | Q15==0
label val Q15_bin worsebetter
order Q15_bin, after (Q15) 

*Q27
label val Q27_1 .
replace Q27_1=0 if Q27_1==1
replace Q27_1=1 if Q27_1==2
replace Q27_1=2 if Q27_1==3
replace Q27_1=3 if Q27_1==4
label def nothreatmod 0 "Not a threat at all" 1 "A little bit of a threat" 2 "A moderate threat" 3 "A significant threat"
label val Q27_1 nothreatmod

gen Q27_1_bin=.
replace Q27_1_bin=1 if Q27_1==2 | Q27_1==3
replace Q27_1_bin=0 if Q27_1==0 | Q27_1==1
label def threatnoyes 0 "No threat" 1 "Significant threat"
label val Q27_1_bin threatnoyes
order Q27_1_bin, after (Q27_1)

gen Q27_2_bin=.
replace Q27_2_bin=1 if Q27_2==2 | Q27_2==3
replace Q27_2_bin=0 if Q27_2==0 | Q27_2==1
label val Q27_2_bin threatnoyes
order Q27_2_bin, after (Q27_2)

gen Q27_3_bin=.
replace Q27_3_bin=1 if Q27_3==2 | Q27_3==3
replace Q27_3_bin=0 if Q27_3==0 | Q27_3==1
label val Q27_3_bin threatnoyes
order Q27_3_bin, after (Q27_3)

*Q29
label val Q29 .
replace Q29=-2 if Q29==1
replace Q29=-1 if Q29==2
replace Q29=0 if Q29==3
replace Q29=1 if Q29==4
replace Q29=2 if Q29==5
label val Q29 worsen

gen Q29_bin=.
replace Q29_bin=1 if Q29==1 | Q29==2
replace Q29_bin=0 if Q29==-2 | Q29==-1 | Q29==0
label val Q29_bin worsebetter
order Q29_bin, after (Q29) 

*Q30_1 - Q30_5
gen Q30_1_bin=.
replace Q30_1_bin=1 if Q30_1==4 | Q30_1==5
replace Q30_1_bin=0 if Q30_1==1 | Q30_1==2 | Q30_1==3
label val Q30_1_bin worsebetter
order Q30_1_bin, after (Q30_1)

gen Q30_2_bin=.
replace Q30_2_bin=1 if Q30_2==4 | Q30_2==5
replace Q30_2_bin=0 if Q30_2==1 | Q30_2==2 | Q30_2==3
label val Q30_2_bin worsebetter
order Q30_2_bin, after (Q30_2)
 
gen Q30_3_bin=.
replace Q30_3_bin=1 if Q30_3==4 | Q30_3==5
replace Q30_3_bin=0 if Q30_3==1 | Q30_3==2 | Q30_3==3
label val Q30_3_bin worsebetter
order Q30_3_bin, after (Q30_3)
 
gen Q30_4_bin=.
replace Q30_4_bin=1 if Q30_4==4 | Q30_4==5
replace Q30_4_bin=0 if Q30_4==1 | Q30_4==2 | Q30_4==3
label val Q30_4_bin worsebetter
order Q30_4_bin, after (Q30_4)
 
gen Q30_5_bin=.
replace Q30_5_bin=1 if Q30_5==4 | Q30_5==5
replace Q30_5_bin=0 if Q30_5==1 | Q30_5==2 | Q30_5==3
label val Q30_5_bin worsebetter
order Q30_5_bin, after (Q30_5)

*Q33
label val Q33 .
replace Q33=0 if Q33==1
replace Q33=1 if Q33==2
replace Q33=2 if Q33==3
replace Q33=3 if Q33==5
replace Q33=4 if Q33==6
replace Q33=5 if Q33==4
label def neverdaily 0 "Never" 1 "Yearly or less" 2 "Monthly" 3 "Weekly" 4 "Daily" 5 "Hourly"
label val Q33 neverdaily

gen Q33_bin=.
replace Q33_bin=1 if Q33==1 | Q33==2 | Q33==3 | Q33==4 | Q33==5
replace Q33_bin=0 if Q33==0 
label def atleasto 0 "Never" 1 "Yearly or less/Monthly/Weekly/Daily/Hourly"
label val Q33_bin atleasto
order Q33_bin, after (Q33)

*Q33a_bin
gen Q33a_bin=.
replace Q33a_bin=1 if Q33a_1==1 | Q33a_2==1 | Q33a_3==1 | Q33a_4==1 | Q33a_5==1
replace Q33a_bin=0 if Q33a_1==2 | Q33a_2==2 | Q33a_3==2 | Q33a_4==2 | Q33a_5==2
label var Q33a_bin "Did you encounter messages for ISIS/AlQ/ENVEO?"
label def yesnoo 0 "No" 1 "Yes"
label val Q33a_bin yesnoo
order Q33a_bin, after (Q33)

*Q37
label val Q37 .
replace Q37=0 if Q37==1
replace Q37=1 if Q37==2
replace Q37=2 if Q37==3
replace Q37=3 if Q37==4
replace Q37=4 if Q37==5
label def neveralways 0 "Never" 1 "Rarely" 2 "Sometimes" 3 "Often" 4 "Always"
label val Q37 neveralways
 
gen Q37_bin=.
replace Q37_bin=1 if Q37==3 | Q37==4
replace Q37_bin=0 if Q37==0 | Q37==1 | Q37==2
label def seldomal 0 "Seldomly" 1 "Often/Always"
label val Q37_bin seldomal
order Q37_bin, after (Q37) 

*Q40
label val Q40 .
replace Q40=0 if Q40==1
replace Q40=1 if Q40==2
replace Q40=2 if Q40==3
replace Q40=3 if Q40==4
replace Q40=4 if Q40==5
label def strongbelieve 0 "Not religious and I’m against religion" 1 "Not religious but I don’t have anything against religion" 2 "I’m not sure if I’m a believer or not" 3 "Religious but I do not accept everything my religious leaders teach" 4 "Believer and I accept everything my religious leaders teach"
label val Q40 strongbelieve

gen Q40_bin=.
replace Q40_bin=1 if Q40==4
replace Q40_bin=0 if Q40==0 | Q40==1 | Q40==2 | Q40==3
label def strongbeliever 0 "Not very religious" 1 "Strongly religious"
label val Q40_bin strongbeliever
order Q40_bin, after (Q40) 

*Q41
label val Q41 .
replace Q41=0 if Q41==1
replace Q41=1 if Q41==2
replace Q41=2 if Q41==3
replace Q41=3 if Q41==4
replace Q41=4 if Q41==5
label def praytime 0 "Never" 1 "A few times a year" 2 "Monthly" 3 "Weekly" 4 "Daily or more"
label val Q41 praytime

gen Q41_bin=.
replace Q41_bin=1 if Q41==3 | Q41==4
replace Q41_bin=0 if Q41==0 | Q41==1 | Q41==2 
label def strongpray 0 "Not often" 1 "Often"
label val Q41_bin strongpray
order Q41_bin, after (Q41)

*Q42 
label val Q42 .
replace Q42=0 if Q42==1
replace Q42=1 if Q42==2
replace Q42=2 if Q42==3
replace Q42=3 if Q42==4
replace Q42=4 if Q42==5
label def likelihood 0 "Not at all likely" 1 "A little bit likely" 2 "Moderately/Somewhat likely" 3 "Very likely" 4 "Definitely"
label val Q42 likelihood

gen Q42_bin=.
replace Q42_bin=1 if Q42==3 | Q42==4
replace Q42_bin=0 if Q42==0 | Q42==1 | Q42==2 
label val Q42_bin likely
order Q42_bin, after (Q42)

*Q44
label val Q44 .
replace Q44=0 if Q44==1
replace Q44=1 if Q44==2
replace Q44=2 if Q44==3
replace Q44=3 if Q44==4
replace Q44=4 if Q44==5

label val Q44 neveralways
 
gen Q44_bin=.
replace Q44_bin=1 if Q44==3 | Q44==4
replace Q44_bin=0 if Q44==0 | Q44==1 | Q44==2
label val Q44_bin seldomal
order Q44_bin, after (Q44) 

*Q46
gen Q46_bin_jihad=.
replace Q46_bin_jihad=1 if Q46==2 | Q46==3
replace Q46_bin_jihad=0 if Q46==1
label def jihad 0 "Moral struggle" 1 "Jihad taking up arms / Both" 
label val Q46_bin_jihad jihad
order Q46_bin_jihad, after(Q46)
label var Q46_bin_jihad "Jihad - taking up arms against enemies of Islam, or both?"

*Q55
label val Q55 .
replace Q55=0 if Q55==1
replace Q55=1 if Q55==2
replace Q55=2 if Q55==3
replace Q55=3 if Q55==4
replace Q55=4 if Q55==5
replace Q55=5 if Q55==6
replace Q55=6 if Q55==7
replace Q55=7 if Q55==8
label def educationq 0 "Not completed any level of education and I cannot read or write" 1 "Not completed any level of education but I can read and write" 2 "Successfully completed elementary education (6th grade)" 3 "Successfully completed primary education (10th grade)" 4 "Successfully completed secondary education- high school (12th grade)" 5 "Successfully completed vocational education or higher diploma degree" 6 "Successfully completed a bachelor's degree" 7 "Successfully completed Master's degree or higher"
label val Q55 educationq

gen Q55_bin=.
replace Q55_bin=1 if Q55==5 | Q55==6 | Q55==7
replace Q55_bin=0 if Q55==0 | Q55==1 | Q55==2 | Q55==3 | Q55==4 
label def educfin 0 "High school diploma or lower degree" 1 "Vocational education or higher degree"
label val Q55_bin educfin
order Q55_bin, after (Q55) 

*Q57
label val Q57 .
replace Q57=0 if Q57==1
replace Q57=1 if Q57==2
replace Q57=2 if Q57==3
replace Q57=3 if Q57==4
label def seldomdaily 0 "Seldom or never" 1 "At least once a month" 2 "At least once a week" 3 "Daily"
label val Q57 seldomdaily
 
gen Q57_bin_daily=.
replace Q57_bin_daily=1 if Q57==3
replace Q57_bin_daily=0 if Q57==0 | Q57==1 | Q57==2
label def internetuse 0 "Seldom" 1 "Daily"
label val Q57_bin_daily internetuse
order Q57_bin_daily, after (Q57) 

*ORDER variables
order age_category, after (age)
order enumerator_gender, after (enumerator_username)

*Save
save "$dirfolder\Data\Midline\01_preprocessing\MK_CVE_BaselineSurvey_2020_Macedonia_prep.dta", replace
