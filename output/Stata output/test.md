
Let us read the fuel efficiency data that is shipped with Stata


{{1}}


To study how fuel efficiency depends on weight it is useful to
transform the dependent variable from "miles per gallon" to
"gallons per 100 miles"


{{2}}


We then obtain a more linear relationship


{{3}}


![Fuel Efficiency](auto.png)

That's all for now!

