---
title: "eco-circumstance"
author: "dkillian"
date: "2021-01-21"
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---

#####

```{r global_options, include=T, warning=F, message=F, echo=T, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=8, fig.height=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/Macedonia analysis prep.R")

dat <- read_dta("data/prepared/N Macedonia prepared.dta")

datNames <- data.frame(names(dat))

# svydat <- svydesign(data = dat,
#                     ids= ~PSU + A9,
#                     strata = ~UK7,
#                     weights = ~RegionalWeight)
# 
# svyrdat <- dat %>%
#   as_survey_design(ids= c(PSU,A9),
#                    strata=strata,
#                    weights=Wgt1)

```
