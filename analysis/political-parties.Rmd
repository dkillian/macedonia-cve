---
title: "Political Parties"
author: "Dan Killian"
date: "2021-03-08"
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---

#####

```{r global_options, include=T, warning=F, message=F, echo=T, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=9, fig.height=5, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/Macedonia analysis prep.R")

```

#### Political parties

```{r}
#frq(dat$Q31_bin)

DUI <- svymean(~DUI,
                 na.rm=T,
                 design=svydat) %>%
  as.data.frame() %>%
  mutate(Preference="DUI",
         Margin = 1.96*DUI) %>%
  select(Preference, Percent=mean, Margin) %>%
  remove_rownames()

#DUI

```

```{r}
#frq(dat$Q31_bin)

SDSM <- svymean(~SDSM,
                 na.rm=T,
                 design=svydat) %>%
  as.data.frame() %>%
  mutate(Preference="SDSM",
         Margin = 1.96*SDSM) %>%
  select(Preference, Percent=mean, Margin) %>%
  remove_rownames()

#SDSM

```

```{r}
#frq(dat$Q31_bin)

VMRO <- svymean(~VMRO_DPMNE,
                 na.rm=T,
                 design=svydat) %>%
  as.data.frame() %>%
  mutate(Preference="VMRO_DPMNE",
         Margin = 1.96*VMRO_DPMNE) %>%
  select(Preference, Percent=mean, Margin) %>%
  remove_rownames()

#VMRO

```

```{r}
#frq(dat$Q31_bin)

All_Alb <- svymean(~AllianceAlb,
                 na.rm=T,
                 design=svydat) %>%
  as.data.frame() %>%
  mutate(Preference="Alliance for Albanians",
         Margin = 1.96*AllianceAlb) %>%
  select(Preference, Percent=mean, Margin) %>%
  remove_rownames()

#All_Alb

```

```{r}
q58 <- do.call(rbind, list(DUI, SDSM, VMRO, All_Alb))

q58 %>%
  gt() %>%
  fmt_percent(2:3, decimals=1)

```

#### Political party preference, by ethnicity

```{r}

DUI_eth <- svyrdat %>%
  group_by(eth_chr) %>%
  summarise(Percent = survey_mean(DUI, na.rm=T)) %>%
  na.omit() %>%
  mutate(Preference="DUI",
         Margin = 1.96*Percent_se) %>%
  select(Preference, Ethnicity=eth_chr, Percent, Margin)
    
#DUI_eth

```

```{r}

SDSM_eth <- svyrdat %>%
  group_by(eth_chr) %>%
  summarise(Percent = survey_mean(SDSM, na.rm=T)) %>%
  na.omit() %>%
  mutate(Preference="SDSM",
         Margin = 1.96*Percent_se) %>%
  select(Preference, Ethnicity=eth_chr, Percent, Margin)
    
#SDSM_eth

```

```{r}

VMRO_eth <- svyrdat %>%
  group_by(eth_chr) %>%
  summarise(Percent = survey_mean(VMRO_DPMNE, na.rm=T)) %>%
  na.omit() %>%
  mutate(Preference="VMRO_DPMNE",
         Margin = 1.96*Percent_se) %>%
  select(Preference, Ethnicity=eth_chr, Percent, Margin)
    
#VMRO_eth

```

```{r}

All_Alb_eth <- svyrdat %>%
  group_by(eth_chr) %>%
  summarise(Percent = survey_mean(AllianceAlb, na.rm=T)) %>%
  na.omit() %>%
  mutate(Preference="Alliance for Albanians",
         Margin = 1.96*Percent_se) %>%
  select(Preference, Ethnicity=eth_chr, Percent, Margin)
    
#All_Alb_eth

```



```{r}

q58_eth <- do.call(rbind, list(DUI_eth, SDSM_eth, VMRO_eth, All_Alb_eth)) %>%
  select(-Margin) %>%
  pivot_wider(names_from=Ethnicity,
              values_from=Percent)

#q58_eth

q58_eth_gt <- q58_eth %>%
  gt() %>%
  fmt_percent(2:3, decimals=1)

q58_eth_gt

#gtsave(q58_eth_gt, "output/tables/q58 political party preference ethnicity.rtf")

```


#### Determinants of support for MVRO (Macedonian, right-wing)

```{r}

vmro1 <- svyglm(VMRO_DPMNE ~ rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate + id_country + ec_marg + Q37 + NoAgency + Rel_Devotion + Q2a_grps + WomEmp + acceptance_gap + Q6help + EU + Services + bribe,
         design=svydat,
         family="binomial")

summary(vmro1)

```

```{r}

plotreg(vmro1,
        omit.coef=("(Intercept)"))


```

#### Determinants of support for Alliance for Albanians (right-wing)

```{r}

all1 <- svyglm(AllianceAlb ~ rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate + id_country + ec_marg + Q37 + NoAgency + Rel_Devotion + Q2a_grps + WomEmp + acceptance_gap + Q6help + EU + Services + bribe,
         design=svydat,
         family="binomial")

summary(all1)

```

```{r}

plotreg(all1,
        omit.coef=("(Intercept)"))


```


##### Variable importance

```{r}
library(randomForest)

out <- dat %>%
  select(AllianceAlb, rural, age, female, never_married, educ, Q54_religschool, ses, internet_use, vic_wit_sum, VEO_message_rate, id_country, id_self, ec_marg, Q37, NoAgency, Rel_Devotion, Q2a_grps, WomEmp, acceptance_gap, Q6help, EU, Services, bribe) %>%
  na.omit()

rf_all <- randomForest(as.factor(AllianceAlb) ~ rural + age + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate + id_country + ec_marg + Q37 + NoAgency + Rel_Devotion + Q2a_grps + WomEmp + acceptance_gap + Q6help + EU + Services + bribe,
                       data=out)

rf_all_imp <- data.frame(importance(rf_all)) %>%
  rownames_to_column("measure") %>%
  arrange(desc(MeanDecreaseGini))

rf_all_imp

```





