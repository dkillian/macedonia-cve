---
title: "VEO sympathy"
author: "Dan Killian"
date: "2021-01-21"
output: 
  workflowr::wflow_html:
    code_folding: hide
editor_options:
  chunk_output_type: console
---

#####

```{r global_options, include=T, warning=F, message=F, echo=T, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=8, fig.height=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/Macedonia analysis prep.R")

```

#### Base models

```{r}

veo_bin <- svyglm(Q31ab_positive ~ rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate,
         design=svydat)

summary(veo_bin)

```

```{r}

plotreg(veo_bin,
        omit.coef=("(Intercept)"))


```

```{r}

veo_log <- svyglm(Q31ab_positive ~ rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate,
                 family="binomial",
                 design=svydat)

summary(veo_log)

```

```{r}

plotreg(veo_log,
        omit.coef=("(Intercept)"))


```

#### BRAVE overall, no controls

```{r}

veo_bin_brv <- svyglm(Q31ab_positive ~ brave,
         design=svydat)

summary(veo_bin_brv)

```

```{r}

plotreg(veo_bin_brv,
        omit.coef=("(Intercept)"))


```

```{r}

veo_log_brv <- svyglm(Q31ab_positive ~ brave,
                 family="binomial",
                 design=svydat)

summary(veo_log_brv)

```

```{r}

plotreg(veo_log_brv,
        omit.coef=("(Intercept)"))


```


#### BRAVE overall, with controls

##### linear probability model

```{r}

veo_bin_brv2 <- svyglm(Q31ab_positive ~ brave + rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate,
         design=svydat)

summary(veo_bin_brv2)

```

```{r}

plotreg(veo_bin_brv2,
        omit.coef=("(Intercept)"))


```

##### logistic model

```{r}

veo_log_brv2 <- svyglm(Q31ab_positive ~ brave + rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate,
                 family="binomial",
                 design=svydat)

summary(veo_log_brv2)

```

```{r}

plotreg(veo_log_brv2,
        omit.coef=("(Intercept)"))


```

#### BRAVE overall, with controls, interacted

```{r}

veo_log_brv3 <- svyglm(Q31ab_positive ~ brave + rural + age + age_sq + female + never_married + educ + Q54_religschool:brave + ses + internet_use*brave + vic_wit_sum + VEO_message_rate,
                 family="binomial",
                 design=svydat)

summary(veo_log_brv3)

```

```{r}

plotreg(veo_log_brv3,
        omit.coef=("(Intercept)"))


```


#### BRAVE factors, no controls

```{r}

veo_bin_brv4 <- svyglm(Q31ab_positive ~ cultural_identity + bridging_capital + violence_behaviors + violence_beliefs + linking_capital,
         design=svydat)

summary(veo_bin_brv4)

```

```{r}

plotreg(veo_bin_brv4,
        omit.coef=("(Intercept)"))


```

```{r}

veo_log_brv4 <- svyglm(Q31ab_positive ~ cultural_identity + bridging_capital + violence_behaviors + violence_beliefs + linking_capital,
                 family="binomial",
                 design=svydat)

summary(veo_log_brv4)

```

```{r}

plotreg(veo_log_brv4,
        omit.coef=("(Intercept)"))


```


#### BRAVE factors, with controls

```{r}

veo_bin_brv5 <- svyglm(Q31ab_positive ~ cultural_identity + bridging_capital + violence_behaviors + violence_beliefs + linking_capital + rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate,
         design=svydat)

summary(veo_bin_brv5)

```

```{r}

plotreg(veo_bin_brv5,
        omit.coef=("(Intercept)"))


```

```{r}

veo_log_brv5 <- svyglm(Q31ab_positive ~ cultural_identity + bridging_capital + violence_behaviors + violence_beliefs + linking_capital + rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate,
                 family="binomial",
                 design=svydat)

summary(veo_log_brv5)

```

```{r}

plotreg(veo_log_brv5,
        omit.coef=("(Intercept)"))


```



#### Full model
```{r}

f1_veo <- svyglm(Q31ab_positive ~ rural + age + age_sq + female + never_married + educ + Q54_religschool + ses + internet_use + vic_wit_sum + VEO_message_rate + id_country + id_self + ec_marg + Q37:as.factor(ethnicity2) + NoAgency + Rel_Devotion + Q2a_grps + WomEmp + acceptance_gap + Q6help + EU + Services + bribe,
         design=svydat,
         family="binomial")

summary(f1_veo)

```

```{r}

plotreg(f1_veo,
        omit.coef=("(Intercept)"))


```

```{r}

f1_veo_eth <- svyglm(Q31ab_positive ~ rural:eth_chr + age:eth_chr + age_sq:eth_chr + female:eth_chr + never_married:eth_chr + educ:eth_chr + Q54_religschool:eth_chr + ses:eth_chr + internet_use:eth_chr + vic_wit_sum:eth_chr + VEO_message_rate:eth_chr + id_country:eth_chr + id_self:eth_chr + ec_marg:eth_chr + Q37:eth_chr + NoAgency:eth_chr + Rel_Devotion:eth_chr + Q2a_grps:eth_chr + WomEmp:eth_chr + acceptance_gap:eth_chr + trust_relig_gap:eth_chr + Q6help:eth_chr + EU:eth_chr + Services:eth_chr + bribe:eth_chr,
         design=svydat,
         family="binomial")


summary(f1_veo_eth)

```

```{r}

plotreg(f1_veo_eth,
        omit.coef=("(Intercept)"))

```

