---
title: "Exploratory analysis"
author: "Dan Killian"
date: "2021-01-21"
output: 
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

[Violence landscape](violence-landscape.html)

[Exposure to violence](violence-exposure.html)

[VEO messaging](veo-messaging.html)

[Sociality](sociality.html)

[Marginalization](marginalization.html)

[Economic circumstances](eco-circumstance.html)

[Public services](services.html)

[COVID-19](covid.html)

[BRAVE index](brave.html)

[VE tactics sympathy](ve-tac-explore.html)

[VE ideological sympathy](ve-ideology-explore.html)

[VEO sympathy](veo-sympathy-explore.html)

[Ethno-nationalist sympathy](ethno-sympathy-explore.html)


