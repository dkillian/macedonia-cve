---
title: "VE Ideological Sympathy"
author: "Dan Killian"
date: "2021-02-16"
output: 
  workflowr::wflow_html:
    code_folding: hide
editor_options:
  chunk_output_type: console
---

#####

```{r global_options, include=T, warning=F, message=F, echo=T, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=9, fig.height=5, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

source("code/Macedonia analysis prep.R")

```


#### Ideological sympathy

```{r}

frq(dat$jihad_ord)
frq(dat$Q46)

ideo <- svymean(~jihad,
                 na.rm=T,
                 design=svydat) %>%
  as.data.frame() %>%
  mutate(Item="Jihad is taking up arms",
         Margin = 1.96*jihad) %>%
  select(Item, Percent=mean, Margin)

#ideo

```

```{r}
ideo_ord <- svymean(~as.factor(Q46),
                 na.rm=T,
                 design=svydat) %>%
  as.data.frame() %>%
  mutate(Item=c("Jihad is an internal moral struggle for improvement", "Jihad is taking up arms against the enemies of Islam","Jihad is both"),
         Margin = 1.96*SE) %>%
  select(Item, Percent=mean, Margin)

#ideo_ord 

ideo_ord_gt <- ideo_ord %>%
  gt() %>%
  fmt_percent(2:3, decimals=1) %>%
  tab_header("Q46. Definition of jihad") %>%
  tab_source_note("Excludes nonresponse")

ideo_ord_gt

#gtsave(ideo_ord_gt, "output/tables/q46 jihad ord.rtf")

```

#### Ideological sympathy, by sampling area

```{r}

jihad_boostnat <- svyrdat %>%
  group_by(boost_nat) %>%
  summarise(Percent = survey_mean(jihad, na.rm=T)) %>%
  na.omit() %>%
  #left_join(eth_key) %>%
  mutate(Item="Jihad is taking up arms",
         Margin = 1.96*Percent_se) %>%
  select(Item, `Sampling area` = boost_nat, Percent, Margin)
    
jihad_boostnat

```

